<?php

namespace yiivgeny\api\yandex;

class Client
{

    /**
     * Decodes the given JSON string into a PHP data structure.
     * @param string $json the JSON string to be decoded
     * @param boolean $asArray whether to return objects in terms of associative arrays.
     * @return mixed the PHP data
     * @throws \BadMethodCallException if there is any decoding error
     */
    public static function jsonDecode($json, $asArray = true)
    {
        if (is_array($json)) {
            throw new \BadMethodCallException('Invalid JSON data.');
        }
        $decode = json_decode((string)$json, $asArray);
        if ($decode === null) {
            static::jsonHandleError(json_last_error());
        }

        return $decode;
    }

    /**
     * Handles [[encode()]] and [[decode()]] errors by throwing exceptions with the respective error message.
     *
     * @param integer $lastError error code from [json_last_error()](http://php.net/manual/en/function.json-last-error.php).
     * @throws \BadMethodCallException
     */
    private static function jsonHandleError($lastError)
    {
        switch ($lastError) {
            case JSON_ERROR_NONE:
                break;
            case JSON_ERROR_DEPTH:
                throw new \BadMethodCallException('The maximum stack depth has been exceeded.');
            case JSON_ERROR_CTRL_CHAR:
                throw new \BadMethodCallException('Control character error, possibly incorrectly encoded.');
            case JSON_ERROR_SYNTAX:
                throw new \BadMethodCallException('Syntax error.');
            case JSON_ERROR_STATE_MISMATCH:
                throw new \BadMethodCallException('Invalid or malformed JSON.');
            case JSON_ERROR_UTF8:
                throw new \BadMethodCallException('Malformed UTF-8 characters, possibly incorrectly encoded.');
            default:
                throw new \BadMethodCallException('Unknown JSON decoding error.');
        }
    }
}
