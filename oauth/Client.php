<?php

namespace yiivgeny\api\yandex\oauth;

use GuzzleHttp\Client AS HTTPClient;

class Client extends \yiivgeny\api\yandex\Client {

    const AUTHORIZE_URL = 'https://oauth.yandex.ru/authorize';
    const GET_TOKEN_URL = 'https://oauth.yandex.ru/token';

    public $clientId;
    public $password;

    public function __construct($clientId = null, $password = null) {
        $this->clientId = $clientId;
        $this->password = $password;
    }

    public function getAuthURL($byCode = true, $displayPopup = false, $state = null) {

        $requestParams['client_id'] = $this->clientId;
        if ($byCode) {
            $requestParams['response_type'] = 'code';
        }
        else {
            $requestParams['response_type'] = 'token';
            if ($displayPopup) {
                $requestParams['display'] = 'popup';
            }
        }
        if ($state !== null) {
            $requestParams['state'] = $state;
        }

        $url = self::AUTHORIZE_URL.'?'.http_build_query($requestParams, NULL, '&', PHP_QUERY_RFC3986);

        return $url;
    }

    public function extractCodeFromRequest($request = array()) {
        if (empty($request)) {
            $request = &$_REQUEST;
        }

        if (isset($request['error'])) {
            throw new Exception($request['error'], 0, isset($request['error_description'])?$request['error_description']:null);
        }

        if (empty($request['code'])) {
            throw new Exception('Bad OAuth response');
        }

        return $request['code'];
    }

    public function getTokenByCode($code) {
        $requestParams = array(
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => $this->clientId,
            'client_secret' => $this->password
        );

        $HTTP = new HTTPClient;

        try {
            $response = $HTTP->post(self::GET_TOKEN_URL, [
                'body' => $requestParams
            ]);
        }
        catch (\GuzzleHttp\Exception\ClientException $E){
            $response = $E->getResponse();
        }

        $answer = $response->json();

        if (isset($answer['error'])) {
            $message = $answer['error'];
            if (isset($answer['error_description'])){
                $message .= ": ".$answer['error_description'];
            }
            throw new Exception($message, -1);
        }

        return $answer;
    }

}
