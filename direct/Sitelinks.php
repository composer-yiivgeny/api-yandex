<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class Sitelinks extends Request
{

    public function getUrl()
    {
        return 'sitelinks';
    }

    public function get($params)
    {
        $params += [
            'FieldNames' => [
                'Id',
                'Sitelinks',
            ],
        ];

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }
    
}
