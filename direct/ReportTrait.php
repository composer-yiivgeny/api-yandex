<?php
/**
 * Copyright (c) 2017. FCMS
 *
 * @author Evgeny Blinov <e.a.blinov@gmail.com>
 * @author Maxim Gimatov <gimatov.maxim@gmail.com >
 */

namespace yiivgeny\api\yandex\direct;


trait ReportTrait
{
    protected $processingMode;

    /**
     * @return string
     */
    public function getProcessingMode()
    {
        return $this->processingMode ?: Reports::PROCESSING_MODE_AUTO;
    }

    /**
     * @param $mode
     * @return ReportTrait
     */
    public function setProcessingMode($mode)
    {
        // TODO: Валидация
        $this->processingMode = $mode;
        return $this;
    }

    protected $returnMoneyInMicros = true;

    /**
     * @return bool
     */
    public function getReturnMoneyInMicros()
    {
        return (bool)$this->returnMoneyInMicros;
    }

    /**
     * @param bool $value
     * @return ReportTrait
     */
    public function returnMoneyInMicros($value = true)
    {
        $this->returnMoneyInMicros = $value;
        return $this;
    }

    protected $skipReportHeader = false;

    /**
     * @return bool
     */
    public function getSkipReportHeader()
    {
        return (bool)$this->skipReportHeader;
    }

    /**
     * @param bool $value
     * @return ReportTrait
     */
    public function skipReportHeader($value = true)
    {
        $this->skipReportHeader = $value;
        return $this;
    }

    protected $skipColumnHeader = false;

    /**
     * @return bool
     */
    public function getSkipColumnHeader()
    {
        return (bool)$this->skipColumnHeader;
    }

    /**
     * @param bool $value
     * @return ReportTrait
     */
    public function skipColumnHeader($value = true)
    {
        $this->skipColumnHeader = $value;
        return $this;
    }

    protected $skipReportSummary = false;

    /**
     * @return bool
     */
    public function getSkipReportSummary()
    {
        return (bool)$this->skipReportSummary;
    }

    /**
     * @param bool $value
     * @return ReportTrait
     */
    public function skipReportSummary($value = true)
    {
        $this->skipReportSummary = $value;
        return $this;
    }

    // TODO: Геттеры

    protected $dateRangeType = 'AUTO';
    protected $dateFrom;
    protected $dateTo;

    public function setPeriod($date, $end = null)
    {
        // TODO: Валидация
        if ($end === null) {
            $this->dateRangeType = $date;
        }
        else {
            $this->dateRangeType = 'CUSTOM_DATE';
            $this->dateFrom = $date;
            $this->dateTo = $end;
        }

        return $this;
    }

    protected $reportType;

    public function setType($type)
    {
        $this->reportType = $type;
        return $this;
    }

    protected $fieldNames = [];

    public function addField($fields)
    {
        $this->fieldNames = array_merge($this->fieldNames, (array)$fields);
        return $this;
    }

    protected $orderBy = [];

    public function addOrder($field, $order = SORT_ASC)
    {
        $this->orderBy[$field] = $order;
        return $this;
    }

    protected $filter = [];

    public function addFilter($field, $operator, $values)
    {
        $this->filter[] = [
            'Field' => $field,
            'Operator' => $operator,
            'Values' => $values,
        ];
        return $this;
    }

    protected $limit;

    public function setLimit($value)
    {
        $this->limit = $value;
        return $this;
    }

    protected $includeVAT;

    public function includeVAT($value = true)
    {
        $this->includeVAT = $value;
        return $this;
    }

    protected $includeDiscount;

    public function includeDiscount($value = true)
    {
        $this->includeDiscount = $value;
        return $this;
    }
}