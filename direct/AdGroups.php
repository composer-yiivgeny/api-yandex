<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class AdGroups extends Request
{
    const STATUS_ACCEPTED       = 'ACCEPTED';
    const STATUS_DRAFT          = 'DRAFT';
    const STATUS_MODERATION     = 'MODERATION';
    const STATUS_PREACCEPTED    = 'PREACCEPTED';
    const STATUS_REJECTED       = 'REJECTED';

    public function getUrl()
    {
        return 'adgroups';
    }

    public function get($params)
    {
        $params += [
            'FieldNames' => [
                'Id',
                'Name',
                'CampaignId',
                'RegionIds',
                'NegativeKeywords',
                'TrackingParams',
                'Status',
                'Type',
                //'MobileAppAdGroup',
                //'DynamicTextAdGroup',
            ],
            'MobileAppAdGroupFieldNames' => [
                'StoreUrl',
                'TargetDeviceType',
                'TargetCarrier',
                'TargetOperatingSystemVersion',
                'AppIconModeration',
                'AppOperatingSystemType',
                'AppAvailabilityStatus',

            ],
            'DynamicTextAdGroupFieldNames' => [
                'DomainUrl',
                'DomainUrlProcessingStatus',
            ],
        ];

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }
    
}
