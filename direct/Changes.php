<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;

/**
 * Class Changes
 * @package yiivgeny\api\yandex\direct
 *
 * @method check(array $params)
 * @method checkCampaigns(array $params)
 */
class Changes extends Request
{

    public function getUrl()
    {
        return 'changes';
    }
}