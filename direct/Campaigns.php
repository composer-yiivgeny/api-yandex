<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class Campaigns extends Request
{
    const STATE_CONVERTED   = 'CONVERTED';
    const STATE_ARCHIVED    = 'ARCHIVED';
    const STATE_SUSPENDED   = 'SUSPENDED';
    const STATE_ENDED       = 'ENDED';
    const STATE_ON          = 'ON';
    const STATE_OFF         = 'OFF';
    const STATE_UNKNOWN     = 'UNKNOWN';

    const STATUS_DRAFT      = 'DRAFT';
    const STATUS_MODERATION = 'MODERATION';
    const STATUS_ACCEPTED   = 'ACCEPTED';
    const STATUS_REJECTED   = 'REJECTED';
    const STATUS_UNKNOWN    = 'UNKNOWN';

    const STATUS_PAYMENT_DISALLOWED = 'DISALLOWED';
    const STATUS_PAYMENT_ALLOWED    = 'ALLOWED';

    public function getUrl()
    {
        return 'campaigns';
    }

    public function get($params, $defaults = true){
        if ($defaults){
            $params += [
                'SelectionCriteria' => new \stdClass(),
                'FieldNames' => [
                    'Id',
                    'Name',
                    'ClientInfo',
                    'StartDate',
                    'EndDate',
                    'TimeTargeting',
                    'TimeZone',
                    'NegativeKeywords',
                    'BlockedIps',
                    'ExcludedSites',
                    'DailyBudget',
                    'Notification',
                    'Type',
                    'Status',
                    'State',
                    'StatusPayment',
                    'StatusClarification',
                    'SourceId',
                    'Statistics',
                    'Currency',
                    'Funds',
                    'RepresentedBy',
                    //'TextCampaign',
                    //'DynamicTextCampaign',
                    //'MobileAppCampaign',
                ],
                'TextCampaignFieldNames' => [
                    'Settings',
                    'BiddingStrategy',
                    'CounterIds',
                    'RelevantKeywords',
                ],
                'MobileAppCampaignFieldNames' => [
                    'Settings',
                    'BiddingStrategy',
                ],
                'DynamicTextCampaignFieldNames' => [
                    'Settings',
                    'CounterIds',
                    'BiddingStrategy',
                ],
            ];
        }

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }

}
