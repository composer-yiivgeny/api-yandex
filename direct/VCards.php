<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class VCards extends Request
{

    public function get($params)
    {
        $params += [
            'FieldNames' => [
                'Id',
                'CampaignId',
                'Country',
                'City',
                'WorkTime',
                'Phone',
                'Street',
                'House',
                'Building',
                'Apartment',
                'InstantMessenger',
                'CompanyName',
                'ExtraMessage',
                'ContactEmail',
                'Ogrn',
                'MetroStationId',
                'PointOnMap',
                'ContactPerson',
            ],
        ];

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }

    public function addEx($params){
        echo 'Call ', __METHOD__, ': ', count($params), " items\n";
        $result = $this->add(['VCards' => $params]);
        if (isset($result['AddResults'])){
            return $result['AddResults'];
        }
        else {
            throw new Exception;
        }
    }

    public function getUrl()
    {
        return 'vcards';
    }
}