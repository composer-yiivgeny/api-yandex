<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class Ads extends Request
{
    const STATE_SUSPENDED           = 'SUSPENDED';
    const STATE_OFF_BY_MONITORING   = 'OFF_BY_MONITORING';
    const STATE_ON                  = 'ON';
    const STATE_OFF                 = 'OFF';
    const STATE_ARCHIVED            = 'ARCHIVED';

    const STATUS_DRAFT          = 'DRAFT';
    const STATUS_MODERATION     = 'MODERATION';
    const STATUS_PREACCEPTED    = 'PREACCEPTED';
    const STATUS_ACCEPTED       = 'ACCEPTED';
    const STATUS_REJECTED       = 'REJECTED';

    public function getUrl()
    {
        return 'ads';
    }

    public function get($params)
    {
        $params += [
            'SelectionCriteria' => new \stdClass(),
            'FieldNames' => [
                'Id',
                'CampaignId',
                'AdGroupId',
                'Status',
                'StatusClarification',
                'State',
                'AdCategories',
                'AgeLabel',
                'Type',
                //'TextAd',
                //'MobileAppAd',
                //'DynamicTextAd',
            ],
            'TextAdFieldNames' => [
                'Title',
                'Text',
                'Href',
                'DisplayDomain',
                'Mobile',
                'VCardId',
                'SitelinkSetId',
                'AdImageHash',
                'VCardModeration',
                'SitelinksModeration',
                'AdImageModeration',
            ],
            'MobileAppAdFieldNames' => [
                'Title',
                'Text',
                'Features',
                'TrackingUrl',
                'Action',
            ],
            'DynamicTextAdFieldNames' => [
                'Text',
                'VCardId',
                'SitelinkSetId',
                'VCardModeration',
                'SitelinksModeration',
            ],
        ];

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }
    
}
