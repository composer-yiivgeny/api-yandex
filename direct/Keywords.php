<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class Keywords extends Request
{
    const STATE_DRAFT       = 'DRAFT';
    const STATE_ACCEPTED    = 'ACCEPTED';
    const STATE_REJECTED    = 'REJECTED';
    const STATE_UNKNOWN     = 'UNKNOWN';

    const STATUS_ON         = 'ON';
    const STATUS_SUSPENDED  = 'SUSPENDED';
    const STATUS_OFF        = 'OFF';

    public function getUrl()
    {
        return 'keywords';
    }

    public function get($params, $defaults = true)
    {
        if ($defaults){
            $params += [
                'FieldNames' => [
                    'Id',
                    'AdGroupId',
                    'CampaignId',
                    'Keyword',
                    'UserParam1',
                    'UserParam2',
                    'Bid',
                    'ContextBid',
                    'StrategyPriority',
                    'Status',
                    'State',
                    'ServingStatus',
                    'Productivity',
                    'StatisticsSearch',
                    'StatisticsNetwork',
                ],
            ];
        }

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }
}
