<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 18:19
 */

namespace yiivgeny\api\yandex\direct;

use GuzzleHttp\Exception\ClientException;
use yiivgeny\api\yandex\direct\ver4\Client as Client4;
use yiivgeny\api\yandex\TokenClient;

/**
 * Class Client
 * @package yiivgeny\api\yandex\direct
 *
 * @method AdGroups adgroups($login = null)
 * @method Ads ads($login = null)
 * @method Bids bids($login = null)
 * @method Campaigns campaigns($login = null)
 * @method Changes changes($login = null)
 * @method Dictionaries dictionaries($login = null)
 * @method ver4/Images images($login = null)
 * @method Keywords keywords($login = null)
 * @method Reports reports($login = null)
 * @method Sitelinks sitelinks($login = null)
 * @method VCards vcards($login = null)
 */
class Client extends TokenClient
{
    const API_URL = 'https://api.direct.yandex.com/json/v5/';
    const API_SANDBOX_URL = 'https://api-sandbox.direct.yandex.com/json/v5/';
    const METHODS_MAP = [
        'adgroups' => AdGroups::class,
        'ads' => Ads::class,
        'bids' => Bids::class,
        'campaigns' => Campaigns::class,
        'changes' => Changes::class,
        'dictionaries' => Dictionaries::class,
        'images' => ver4\Images::class,
        'keywords' => Keywords::class,
        'reports' => Reports::class,
        'sitelinks' => Sitelinks::class,
        'v4' => Client4::class,
        'vcards' => VCards::class,
    ];

    public $map = [];
    public $debug = false;
    public $requests = [];
    protected $last = [];

    protected $HTTPClient;
    protected $language = 'ru';
    protected $login;

    private $_sandbox = false;

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function __construct($token = null, $sandbox = false)
    {
        parent::__construct($token);

        $config['base_url'] = $sandbox ? static::API_SANDBOX_URL : static::API_URL;
        if ($token !== null) {
            $config['defaults']['headers']['Authorization'] = 'Bearer ' . $token;
        }

        $this->_sandbox = $sandbox;
        $this->HTTPClient = new \GuzzleHttp\Client($config);
    }

    public function __call($service, $args)
    {
        if (isset($this->map[$service])) {
            $classname = $this->map[$service];
        }
        elseif (isset(static::METHODS_MAP[$service])) {
            $classname = static::METHODS_MAP[$service];
        }
        else {
            throw new \BadMethodCallException('Неизвестный сервис');
        }

        array_unshift($args, $this);
        $reflection = new \ReflectionClass($classname);
        return $reflection->newInstanceArgs($args);
    }

    /**
     * @return \yiivgeny\api\yandex\direct\ver4\Client
     */
    public function v4()
    {
        if (isset($this->map['v4'])) {
            $classname = $this->map['v4'];
        }
        elseif (isset(static::METHODS_MAP['v4'])) {
            $classname = static::METHODS_MAP['v4'];
        }

        $Client = new $classname($this->token);
        $Client->sandbox = $this->_sandbox;
        return $Client;
    }

    public function request(Request $Request, $params, array $headers = [], array $options = [])
    {
        $this->last = $last = new \stdClass();
        $headers['Accept-Language'] = $this->language;
        if (($login = $Request->getLogin()) !== null) {
            $last->login = $headers['Client-Login'] = $login;
        }
        elseif ($this->login !== null) {
            $last->login = $headers['Client-Login'] = $this->login;
        }

        if ($this->_sandbox){
            $last->login .= '-sandbox';
        }

        if (is_string($params)) {
            $requestData = $params;
        }
        else {
            $last->method = $params['method']??'unknown';
            $requestData = json_encode($params, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }

        try {
            $last->entry = $Request->getUrl();
            $Response = $this->HTTPClient->post($last->entry, [
                'headers' => $headers,
                'body' => $requestData,
            ] + $options);
        }
        catch (ClientException $E) {
            $Response = $E->getResponse();
        }

        $last->request_id = $Response->getHeader('RequestId');
        $last->request = $requestData;
        $last->response = $Response->getBody()->getContents();
        $Response->getBody()->seek(0);

        if ($units = $Response->getHeader('Units')){
            $last->units = (object)array_combine(
                ['spent', 'available', 'allowed'],
                array_map('intval', explode('/', $units, 3))
            );
        }

        if ($this->debug) {
            $this->requests[$last->request_id] = $last;
        }

        $contentType = explode('; ', $Response->getHeader('Content-Type'), 2)[0];

        if ($contentType === 'application/json') {
            $result = static::jsonDecode($last->response);

            if (isset($result['error'])) {
                $error = $result['error'];
                $last->error = (object)[
                    'error' => $error['error_string'],
                    'code' => $error['error_code'],
                ];
                $errorText = $error['error_string'];
                if (!empty($error['error_detail'])) {
                    $last->error->detail = $error['error_detail'];
                    $errorText .= ': ' . $error['error_detail'];
                }
                $errorText .= ' (' . $result['error']['error_code'] . ')';
                throw new Exception($errorText, $result['error']['error_code'], null, $last);
            }

            return $result['result'];
        }
        elseif (isset($E)) {
            $last->error = (object)[
                'error' => $E->getMessage(),
                'code' => $E->getCode(),
            ];
            throw $E;
        }
        else {
            return $Response;
        }

    }

}
