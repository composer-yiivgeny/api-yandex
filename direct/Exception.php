<?php

namespace yiivgeny\api\yandex\direct;

class Exception extends \yiivgeny\api\yandex\Exception
{

    private $debugInfo;

    public function __construct($message, $code, $previous, $debugInfo = null)
    {
        parent::__construct($message, $code, $previous);
        $this->debugInfo = $debugInfo;
    }

    public final function getDebugInfo()
    {
        return $this->debugInfo;
    }

}
