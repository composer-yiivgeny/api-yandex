<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 18:59
 */

namespace yiivgeny\api\yandex\direct;


abstract class Request
{
    /**
     * @var Client
     */
    protected $Client;
    protected $login;

    public function __construct(Client $Client, $login = null)
    {
        $this->Client = $Client;
        $this->login = $login;
    }

    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @inheritDoc
     */
    public function __call($name, $arguments)
    {
        $data['method'] = $name;
        if (isset($arguments[0])) {
            $data['params'] = $arguments[0];
        }

        return $this->Client->request($this, $data);
    }


    abstract public function getUrl();

}