<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class Dictionaries extends Request
{
    const CURRENCIES = 'Currencies';
    const METRO_STATIONS = 'MetroStations';
    const GEO_REGIONS = 'GeoRegions';
    const TIME_ZONES = 'TimeZones';
    const CONSTANTS = 'Constants';
    const AD_CATEGORIES = 'AdCategories';
    const OPERATING_SYSTEM_VERSIONS = 'OperationSystemVersions';
    const PRODUCTIVITY_ASSERTIONS = 'ProductivityAssertions';
    const SUPPLY_SIDE_PLATFORMS = 'SupplySidePlatforms';

    public function get($params = null)
    {
        if ($params === null) {
            $params['DictionaryNames'] = [
                static::CURRENCIES,
                static::METRO_STATIONS,
                static::GEO_REGIONS,
                static::TIME_ZONES,
                static::CONSTANTS,
                static::AD_CATEGORIES,
                static::OPERATING_SYSTEM_VERSIONS,
                static::PRODUCTIVITY_ASSERTIONS,
                static::SUPPLY_SIDE_PLATFORMS,
            ];
        }

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }

    public function getUrl()
    {
        return 'dictionaries';
    }
}