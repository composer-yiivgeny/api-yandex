<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class Reports extends Request
{
    use ReportTrait;

    const PROCESSING_MODE_AUTO = 'auto';
    const PROCESSING_MODE_ONLINE = 'online';
    const PROCESSING_MODE_OFFLINE = 'offline';

    const OPERATOR_EQUALS = 'EQUALS';
    const OPERATOR_NOT_EQUALS = 'NOT_EQUALS';
    const OPERATOR_IN = 'IN';
    const OPERATOR_NOT_IN = 'NOT_IN';
    const OPERATOR_GREATER_THAN = 'GREATER_THAN';
    const OPERATOR_LESS_THAN = 'LESS_THAN';
    const OPERATOR_TARTS_WITH_IGNORE_CASE = 'TARTS_WITH_IGNORE_CASE';
    const OPERATOR_STARTS_WITH_ANY_IGNORE_CASE = 'STARTS_WITH_ANY_IGNORE_CASE';
    const OPERATOR_DOES_NOT_START_WITH_IGNORE_CASE = 'DOES_NOT_START_WITH_IGNORE_CASE';
    const OPERATOR_DOES_NOT_START_WITH_ALL_IGNORE_CASE = 'DOES_NOT_START_WITH_ALL_IGNORE_CASE';

    const DATE_RANGE_TYPE = [
        'TODAY',
        'YESTERDAY',
        'LAST_3_DAYS',
        'LAST_5_DAYS',
        'LAST_7_DAYS',
        'LAST_14_DAYS',
        'LAST_30_DAYS',
        'LAST_90_DAYS',
        'LAST_365_DAYS',
        'THIS_WEEK_MON_TODAY',
        'THIS_WEEK_SUN_TODAY',
        'LAST_WEEK',
        'LAST_BUSINESS_WEEK',
        'LAST_WEEK_SUN_SAT',
        'THIS_MONTH',
        'LAST_MONTH',
        'ALL_TIME',
        'CUSTOM_DATE',
        'AUTO',
    ];
    const REPORT_TYPE = [
        'ACCOUNT_PERFORMANCE_REPORT',
        'CAMPAIGN_PERFORMANCE_REPORT',
        'ADGROUP_PERFORMANCE_REPORT',
        'AD_PERFORMANCE_REPORT',
        'CRITERIA_PERFORMANCE_REPORT',
        'CUSTOM_REPORT',
        'SEARCH_QUERY_PERFORMANCE_REPORT',
    ];
    const FIELD_NAMES = [
        'AdFormat' => [1, 1, 1],
        'AdGroupId' => [1, 1, 1],
        'AdGroupName' => [1, 0, 1],
        'AdId' => [1, 1, 1],
        'AdNetworkType' => [1, 1, 1],
        'Age' => [1, 1, 1],
        'AudienceTargetId' => [0, 1, 0],
        'AvgClickPosition' => [1, 1, 1],
        'AvgCpc' => [1, 1, 1],
        'AvgImpressionPosition' => [1, 1, 1],
        'AvgPageviews' => [1, 1, 1],
        'BounceRate' => [1, 1, 1],
        'Bounces' => [1, 0, 1],
        'CampaignId' => [1, 1, 1],
        'CampaignName' => [1, 0, 1],
        'CampaignType' => [1, 0, 1],
        'CarrierType' => [1, 1, 1],
        'Clicks' => [1, 1, 1],
        'ClickType' => [1, 1, 1],
        'ConversionRate' => [1, 1, 1],
        'Conversions' => [1, 1, 1],
        'Cost' => [1, 1, 1],
        'CostPerConversion' => [1, 1, 1],
        'Criteria' => [1, 0, 0],
        'CriteriaId' => [1, 0, 0],
        'CriteriaType' => [1, 1, 1],
        'Criterion' => [1, 0, 0],
        'CriterionId' => [1, 0, 0],
        'Ctr' => [1, 1, 1],
        'Date' => [1, 0, 1],
        'Device' => [1, 1, 1],
        'DynamicTextAdTargetId' => [0, 1, 0],
        'ExternalNetworkName' => [1, 1, 1],
        'Gender' => [1, 1, 1],
        'GoalsRoi' => [1, 1, 1],
        'Impressions' => [1, 1, 1],
        'ImpressionShare' => [1, 1, 1],
        'Keyword' => [0, 1, 0],
        'LocationOfPresenceId' => [1, 1, 1],
        'LocationOfPresenceName' => [1, 0, 1],
        'MatchedKeyword' => [1, 1, 1],
        'MatchType' => [1, 1, 1],
        'MobilePlatform' => [1, 1, 1],
        'Month' => [1, 0, 1],
        'Placement' => [1, 1, 1],
        'Quarter' => [1, 0, 1],
        'Query' => [1, 1, 1],
        'Revenue' => [1, 1, 1],
        'RlAdjustmentId' => [1, 1, 1],
        'Sessions' => [1, 0, 1],
        'Slot' => [1, 1, 1],
        'SmartBannerFilterId' => [0, 1, 0],
        'TargetingLocationId' => [1, 1, 1],
        'TargetingLocationName' => [1, 0, 1],
        'Week' => [1, 0, 1],
        'Year' => [1, 0, 1],
    ];

    public function getUrl()
    {
        return '/v5/reports';
    }

    public function getClient()
    {
        return $this->Client;
    }

    public function __call($name, $arguments)
    {
        throw new \BadMethodCallException("Unknown method {$name}");
    }

    public function create($name)
    {
        $configuration = [];
        $Reflection = new \ReflectionClass(ReportTrait::class);
        foreach ($Reflection->getProperties() AS $property) {
            $configuration[$property->getName()] = $this->{$property->getName()};
        }

        return new Report($this, $configuration, $name);
    }

}
