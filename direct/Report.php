<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


use GuzzleHttp\Exception\ClientException;

class Report
{
    use ReportTrait;

    protected $service;
    protected $name;

    public function __construct(Reports $service, array $configuration, $name)
    {
        $this->service = $service;
        $this->name = $name;
        foreach ($configuration AS $property => $value) {
            $this->$property = $value;
        }
    }

    protected static function arrayToSimpleXML(\SimpleXMLElement $parent, $data, $prev = null)
    {
        if (is_scalar($data)) {
            $parent[0] = $data;
        }
        else {
            foreach ($data AS $key => $value) {
                if (is_array($value) && array_keys($value) === range(0, count($value) - 1)) {
                    foreach ($value AS $subvalue) {
                        static::arrayToSimpleXML($parent->addChild($key), $subvalue);
                    }
                }
                else {
                    static::arrayToSimpleXML($parent->addChild($key), $value);
                }
            }
        }

    }

    public function getRequestXML()
    {
        $report = [
            'SelectionCriteria' => [],
            'FieldNames' => $this->fieldNames,
        ];

        if ($this->dateFrom !== null) {
            $report['SelectionCriteria']['DateFrom'] = $this->dateFrom;
        }
        if ($this->dateTo !== null) {
            $report['SelectionCriteria']['DateTo'] = $this->dateTo;
        }

        if ($this->filter) {
            $report['SelectionCriteria']['Filter'] = $this->filter;
        }

        if ($this->limit !== null) {
            $report['Page']['Limit'] = $this->limit;
        }

        foreach ($this->orderBy as $field => $direction) {
            $report['OrderBy'][] = [
                'Field' => $field,
                'SortOrder' => ($direction == SORT_DESC) ? 'DESCENDING' : 'ASCENDING',
            ];
        }

        $report += [
            'ReportName' => $this->name,
            'ReportType' => $this->reportType,
            'DateRangeType' => $this->dateRangeType,
            'Format' => 'TSV',
            'IncludeVAT' => $this->includeVAT ? 'YES' : 'NO',
            'IncludeDiscount' => $this->includeDiscount ? 'YES' : 'NO',
        ];

        $Report = new \SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><ReportDefinition xmlns='http://api.direct.yandex.com/v5/reports' />");
        static::arrayToSimpleXML($Report, $report);

        // Форматирование
        $DOM = dom_import_simplexml($Report)->ownerDocument;
        $DOM->formatOutput = true;

        if ($this->service->getClient()->debug) {
            $validation = $DOM->schemaValidate('https://api.direct.yandex.com/v5/reports.xsd');
        }

        return $DOM->saveXML();
    }

    public function getRequestHeaders()
    {
        $headers['processingMode'] = $this->getProcessingMode();

        if ($this->getReturnMoneyInMicros() === false) {
            $headers['returnMoneyInMicros'] = 'false';
        }

        if ($this->getSkipReportHeader()) {
            $headers['skipReportHeader'] = 'true';
        }

        if ($this->getSkipColumnHeader()) {
            $headers['skipColumnHeader'] = 'true';
        }

        if ($this->getSkipReportSummary()) {
            $headers['skipReportSummary'] = 'true';
        }

        return $headers;
    }

    public function request(&$retryIn = null, $file = null)
    {
        $retryIn = null;
        $options = [];
        if ($file !== null){
            $options['save_to'] = $file;
        }
        try {
            $headers = $this->getRequestHeaders();
            $Response = $this->service->getClient()->request($this->service, $this->getRequestXML(), $headers, $options);
            $retryIn = intval($Response->getHeader('retryIn'));

            switch ($Response->getStatusCode()) {
                case 200:
                    return $Response->getBody();
                case 201:
                case 202:
                    return true;
                default:
                    throw new Exception('Unknown status code: ' . $Response->getReasonPhrase(), $Response->getStatusCode());
            }
        }
        catch (ClientException $E) {
            $Body = $E->getResponse()->getBody();
            throw new Exception($Body->getContents(), $E->getCode(), $E);
        }
    }

    public function get($file = null)
    {
        while (true) {
            $result = $this->request($retryIn, $file);
            if ($result === true && $retryIn > 0) {
                sleep($retryIn);
                continue;
            }

            return $result;
        }

    }


}
