<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct;


class Bids extends Request
{

    public function getUrl()
    {
        return 'bids';
    }

    public function get($params, $defaults = true)
    {
        if ($defaults) {
            $params += [
                'FieldNames' => [
                    "CampaignId",
                    "AdGroupId",
                    "KeywordId",
                    "Bid",
                    "ContextBid",
                    "StrategyPriority",
                    "CompetitorsBids",
                    "SearchPrices",
                    "ContextCoverage",
                    "MinSearchPrice",
                    "CurrentSearchPrice",
                    "AuctionBids",
                ]
            ];
        }

        return $this->Client->request($this, [
            'method' => 'get',
            'params' => $params,
        ]);

    }

}
