<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 23.01.2016
 * Time: 19:10
 */

namespace yiivgeny\api\yandex\direct\ver4;


use yiivgeny\api\yandex\direct\Client;

class Images
{

    /**
     * @var Client
     */
    protected $Client;
    protected $login;

    public function __construct(Client $Client, $login = null)
    {
        $this->Client = $Client->v4();
        $this->login = $login;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function get($params = []){
        $params += [
            'SelectionCriteria' => new \stdClass(),
        ];

        return $this->__call('Get', [$params]);
    }

    /**
     * @inheritDoc
     */
    public function __call($name, $arguments)
    {
        if (isset($arguments[0])) {
            $data = $arguments[0];
        }
        else {
            $data = [];
        }
        $data['Action'] = $name;

        return $this->Client->callMethod('AdImage', $data);
    }

}