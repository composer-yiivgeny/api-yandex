<?php

namespace yiivgeny\api\yandex\direct\ver4;

use GuzzleHttp\Client AS HTTPClient;
use yiivgeny\api\yandex\direct\Exception;

class Client extends \yiivgeny\api\yandex\TokenClient {

    protected $apiURLs = [
        'v4sandbox' => 'https://api-sandbox.direct.yandex.ru/live/v4/json/',
        'v4'        => 'https://api.direct.yandex.ru/live/v4/json/',
    ];

    public $sandbox = false;
    public $locale = 'ru';

    /*
     * Параметры финансовых операций
     */
    public $masterToken;
    public $login;

    protected $HTTPClient;

    public function __construct($token) {
        parent::__construct($token);
        $this->HTTPClient = new HTTPClient;
    }

    public function callMethod($method, $param = null, $financeOperation = null, &$request = null) {
        $request = array(
            'locale' => $this->locale,
            'token' => $this->token,
            'method' => $method
        );

        if ($param !== null) {
            $request['param'] = $param;
        }
        if ($financeOperation !== null) {
            if ($financeOperation === false){
                $financeOperation = time();
            }
            $request['finance_token'] = hash('sha256', $this->masterToken.$financeOperation.$method.$this->login);
            $request['operation_num'] = $financeOperation;
        }

        $response = $this->HTTPClient->post($this->apiURLs[$this->sandbox?'v4sandbox':'v4'], [
            'body' => json_encode($request, JSON_UNESCAPED_UNICODE),
        ]);

        $answer = $response->json();

        if (isset($answer['error_code'])) {
            $message = $answer['error_str']." ({$answer['error_code']})";
            if (!empty($answer['error_detail'])){
                $message .= ": ".$answer['error_detail'];
            }
            throw new Exception($message, $answer['error_code'], null, $answer);
        }

        return $answer['data'];
    }

}
