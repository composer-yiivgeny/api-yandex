<?php

namespace yiivgeny\api\yandex\direct\ver4;

use yiivgeny\api\yandex\direct\Exception;

use GuzzleHttp\Exception\ConnectException;

class ClientExtended extends Client {

    public function __call($method, $arguments) {
        while(true){
            try {
                return $this->callMethod($method, isset($arguments[0])?$arguments[0]:null);
            }
            catch (Exception $E) {
                if ($E->getCode() == 52){
                    sleep(3);
                }
                else {
                    throw $E;
                }
            }
            catch (ConnectException $E) {
                sleep(5);
            }
        }
    }
    
    public function GetWaitedForecastReport($reportId, $sleep = 3) {
        while (true) {
            try {
                $data = $this->GetForecast($reportId);
                break;
            }
            catch (Exception $E) {
                if ($E->getCode() !== 74)
                    throw $E;
                if ($sleep > 0)
                    sleep($sleep);
            }
        }
        return $data;
    }

    public function GetWaitedWordstatReport($reportId, $sleep = 3) {
        while (true) {
            try {
                $data = $this->GetWordstatReport($reportId);
                break;
            }
            catch (Exception $E) {
                if ($E->getCode() !== 92)
                    throw $E;
                if ($sleep > 0)
                    sleep($sleep);
            }
        }
        return $data;
    }

    public function cleanForecastReports(){
        $reports = $this->GetForecastList();
        foreach ($reports as $report) {
            if ($report['StatusForecast'] === 'Done'){
                $this->DeleteForecastReport($report['ForecastID']);
            }
        }
        return $reports;
    }
}
