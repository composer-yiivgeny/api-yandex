<?php

namespace yiivgeny\api\yandex\passport;

use GuzzleHttp\Client AS HTTPClient;

class Client extends \yiivgeny\api\yandex\TokenClient {

    const INFO_URL = 'https://login.yandex.ru/info';

    public function getInfo() {
        $HTTPClient = new HTTPClient;

        $response = $HTTPClient->get(self::INFO_URL, [
            'query' => [
                'format' => 'json',
                'oauth_token' => $this->token
            ]
        ]);

        return $response->json();
    }

}
